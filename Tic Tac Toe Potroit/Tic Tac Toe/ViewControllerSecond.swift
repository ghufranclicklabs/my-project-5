//
//  ViewControllerSecond.swift
//  Tic Tac Toe
//
//  Created by clicklabs92 on 04/02/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

class ViewControllerSecond: UIViewController {
    
    //label for counting win, loss, total match and tie matches
    @IBOutlet weak var playerOneWinScore: UILabel!
    @IBOutlet weak var playerTwoWinScore: UILabel!
    @IBOutlet weak var tieScores: UILabel!
    @IBOutlet weak var totalMatchesPlay: UILabel!
    @IBOutlet weak var playerOneNameLabel: UILabel!
    @IBOutlet weak var playerTwoNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var playerOneLossScore: UILabel!
    @IBOutlet weak var playerTwoLossScore: UILabel!
    //total match played
    var totalMatches = playerOneWinMatches + playerSecondWinMatches + totalTieMatches
    //for printing score when score button tapped
    @IBAction func scoreButton(sender: AnyObject) {
        if (totalMatches != 0) {
          playerOneWinScore.text = "\( playerOneWinMatches)"
          playerTwoWinScore.text = "\(playerSecondWinMatches)"
          totalMatches = playerOneWinMatches + playerSecondWinMatches + totalTieMatches
          totalMatchesPlay.text = "\(totalMatches)"
          tieScores.text = "\(totalTieMatches)"
          playerOneLossScore.text  = "\(playerSecondWinMatches)"
          playerTwoLossScore.text  = "\(playerOneWinMatches)"
          playerOneNameLabel.text = firstPlayerNameGlobal
          playerTwoNameLabel.text = secondPlayerNameGlobal
        } else {
            messageLabel.hidden = false
            messageLabel.text = "No Match Played"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      // Do any additional setup after loading the view.
    }
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
